<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTableSengketas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sengketas', function (Blueprint $table) {
            $table->string('seksi')->nullable();
            $table->string('year_started')->nullable();
            $table->string('conclude_year')->nullable();
            $table->string('ended_year')->nullable();
            $table->string('tgl_surat_pemberitahuan_wp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('table_sengketas', function (Blueprint $table) {
            //
        });
    }
}
