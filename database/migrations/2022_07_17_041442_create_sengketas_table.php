<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSengketasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sengketas', function (Blueprint $table) {
            $table->id();
            $table->string('case_id')->nullable();
            $table->string('npwp')->nullable();
            $table->string('nama_npwp')->nullable();
            $table->string('kpp_terdaftar')->nullable();
            $table->string('type_kasus')->nullable();
            $table->date('tgl_permohonan')->nullable();
            $table->string('pihak_afiliasi')->nullable();
            $table->integer('negara_id')->nullable();
            $table->string('covered_term')->nullable();
            $table->string('covered_transaction')->nullable();
            $table->string('no_surat_tugas')->nullable();
            $table->date('tgl_surat_tugas')->nullable();
            $table->string('pic')->nullable();
            $table->date('tgl_kkpt')->nullable();
            $table->string('no_surat_pemberitahuan_wp')->nullable();
            $table->string('no_notification')->nullable();
            $table->date('date_notification')->nullable();
            $table->string('file_surat_permohonan')->nullable();
            $table->string('file_surat_tugas')->nullable();
            $table->string('file_kkpt_formal')->nullable();
            $table->string('file_surat_pemberitahuan')->nullable();
            $table->string('file_notification')->nullable();
            $table->string('no_risalah_komhas')->nullable();
            $table->string('tgl_komhas')->nullable();
            $table->date('tgl_position_paper')->nullable();
            $table->string('no_laporan_penelaahan')->nullable();
            $table->date('tgl_laporan_penelaahan')->nullable();
            $table->string('file_risalah_komhas')->nullable();
            $table->string('file_position_paper')->nullable();
            $table->string('file_laporan_penelaahan')->nullable();
            $table->date('tgl_mutual_agreement')->nullable();
            $table->string('no_sk_map')->nullable();
            $table->string('tgl_sk_map')->nullable();
            $table->string('no_sk_pembetulan')->nullable();
            $table->string('tgl_sk_pembetulan')->nullable();
            $table->string('no_closing_letter_dgt')->nullable();
            $table->date('tgl_closing_letter_dgt')->nullable();
            $table->string('no_closing_letter_camitra')->nullable();
            $table->date('tgl_closing_letter_camitra')->nullable();
            $table->string('file_mutual_agreement')->nullable();
            $table->string('file_upload_sk_map')->nullable();
            $table->string('file_sk_pembetulan')->nullable();
            $table->string('currency')->nullable();
            $table->integer('nilai_cfm_wp')->nullable();
            $table->integer('nilai_cfm_pemeriksa')->nullable();
            $table->integer('koreksi_primer')->nullable();
            $table->integer('nilai_cfm_ma')->nullable();
            $table->integer('downward_adjusment')->nullable();
            $table->integer('refund')->nullable();
            $table->string('oecd_category')->nullable();
            $table->integer('year_category')->nullable();
            $table->date('starting_date')->nullable();
            $table->date('milestone1')->nullable();
            $table->date('end_date')->nullable();
            $table->string('outcome')->nullable();
            $table->string('case_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sengketas');
    }
}
