<footer class="main-footer text-dark">
    <div class="footer-left">
        Copyright &copy; <script>document.write(new Date().getFullYear())</script> <a href="https://instagram.com/berkahbekhan.inc" target="_blank">berkahbekhan.inc</a> All Rights Reserved
    </div>
    <div class="footer-right">
        v1.0
    </div>
</footer>
