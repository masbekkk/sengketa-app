<?php

namespace App\Exports;

use App\Models\Sengketa;
use Maatwebsite\Excel\Excel;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;

class SengketaExport implements FromCollection, Responsable, WithMapping, WithHeadings, ShouldAutoSize, WithTitle, WithStyles
{
    use Exportable;
    /**
    * It's required to define the fileName within
    * the export class when making use of Responsable.
    */
    protected $fileName;
    
    /**
    * Optional Writer Type
    */
    private $writerType = Excel::XLSX;
    
    /**
    * Optional headers
    */
    private $headers = [
        'Content-Type' => 'text/csv',
    ];
    public $now;
    public function __construct($now)
    {
        $this->fileName = 'data-sengketa export tgl ' . $now. '.xlsx';
        $this->now = $now;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Sengketa::with('negara')->get();
    }

    public function map($data) : array
    {
        return [
            $data->case_id,
            $data->npwp,
            $data->nama_npwp,
            $data->kpp_terdaftar,
            $data->type_kasus,
            $data->negara->name,
            $data->seksi,
            $data->tgl_permohonan,
            $data->pihak_afiliasi,
            $data->covered_term,
            $data->covered_transaction,
            $data->file_surat_permohonan,
            $data->no_surat_tugas,
            $data->tgl_surat_tugas,
            $data->pic,
            $data->tgl_kkpt,
            $data->no_surat_pemberitahuan_wp,
            $data->tgl_surat_pemberitahuan_wp,
            $data->no_notification,
            $data->date_notification,
            $data->file_surat_tugas,
            $data->file_kkpt_formal,
            $data->file_surat_pemberitahuan,
            $data->file_notification,
            $data->no_risalah_komhas,
            $data->tgl_komhas,
            $data->tgl_position_paper,
            $data->no_laporan_penelaahan,
            $data->tgl_no_laporan_penelaahan,
            $data->file_risalah_komhas,
            $data->file_position_paper,
            $data->file_laporan_penelaahan,
            $data->tgl_mutual_agreement,
            $data->no_sk_map,
            $data->tgl_sk_map,
            $data->no_sk_pembetulan,
            $data->tgl_sk_pembetulan,
            $data->no_closing_letter_dgt,
            $data->tgl_closing_letter_dgt,
            $data->no_closing_letter_camitra,
            $data->tgl_closing_letter_camitra,
            $data->currency,
            $data->nilai_cfm_wp,
            $data->nilai_cfm_pemeriksa,
            $data->koreksi_primer,
            $data->nilai_cfm_ma,
            $data->downward_adjusment,
            $data->refund,
            $data->oecd_category,
            $data->year_category,
            $data->year_started,
            $data->conclude_year,
            $data->ended_year,
            $data->starting_date,
            $data->milestone1,
            $data->end_date,
            $data->outcome,
            $data->case_status,
            Carbon::parse($data->created_at)->toFormattedDateString(),
            Carbon::parse($data->updated_at)->toFormattedDateString()
        ];
    }

    public function headings() : array 
    {
        return[
            'Case ID',
            'NPWP',
            'Nama WP',
            'KPP Terdaftar',
            'Tipe Kasus',
            'Negara',
            'Seksi',
            'Tanggal Permohonan/ Notification Letter',
            'Pihak Afiliasi',
            'Covered Term',
            'Covered Transaction/ Matter',
            'File Permohonan/ Notification Letter',
            'No. Surat Tugas',
            'Tanggal Surat Tugas',
            'PIC',
            'Tanggal KKPt Formal',
            'No. Surat Pemberitahuan ke WP',
            'Tanggal Surat Pemberitahuan ke WP',
            'No. Notification/ Acknowledgement Letter',
            'Tanggal Notification/ Acknowledgement Letter',
            'File Surat Tugas',
            'File KKPt Formal',
            'File Surat Pemberitahuan',
            'File Notification/ Acknowledgement Letter',
            'No. Risalah Komhas',
            'Tanggal Komhas',
            'Tanggal Position Letter',
            'No. Laporan Penelaahan',
            'Tanggal Laporan Penelaahan',
            'File Risalah Komhas',
            'File Position Paper',
            'File Laporan Penelaahan',
            'Tanggal Mutual Agreement/ Naskah UAPA',
            'No. SK MAP/APA',
            'Tanggal SK MAP/APA',
            'No. SK Pembetulan',
            'Tanggal SK Pembetulan',
            'No. Closing Letter DGT',
            'Tanggal Closing Letter DGT',
            'No. Closing CA Mitra',
            'Tanggal Closing CA Mitra',
            'Currency',
            'Nilai cfm WP',
            'Nilai cfm Pemeriksa',
            'Koreksi Primer',
            'Nilai cfm MA',
            'Downward Adjustment',
            'Refund',
            'OECD Category',
            'Year Category',
            'Started Year',
            'Conclude Year',
            'Ended Year',
            'Starting Date',
            'Milestone 1',
            'End Date',
            'Outcome',
            'Case Status',
            'Tgl Dibuat',
            'Tgl Diupdate'
        ];
    }

    public function title(): string
    {
    	return "Export Tgl $this->now";
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle(1)->getFont()->setBold(true);
    }
}
