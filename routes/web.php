<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SengketaController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('sengketa.index'));
})->name('/');
Route::get('/welcome', function () {
    return view('welcome');
})->name('welcome');
Route::get('/dashboard', [SengketaController::class, 'dashboard'])->name('sengketa.dashboard');
Route::get('/export-data', [SengketaController::class, 'export'])->name('sengketa.export');
Route::get('/sengketa/detail/{id}', [SengketaController::class, 'detail'])->name('sengketa.detail');
Route::get('/sengketa', [SengketaController::class, 'index'])->name('sengketa.index');
Route::get('/sengketa/{sengketum}', [SengketaController::class, 'show'])->name('sengketa.show');
Route::middleware('auth')->group(function () {
    Route::get('/sengketa/create/data', [SengketaController::class, 'create'])->name('sengketa.create');
    Route::post('/sengketa/store', [SengketaController::class, 'store'])->name('sengketa.store');
    Route::get('/sengketa/{sengketum}/edit', [SengketaController::class, 'edit'])->name('sengketa.edit');
    Route::put('/sengketa/{sengketum}', [SengketaController::class, 'update'])->name('sengketa.update');
    Route::delete('/sengketa/{sengketum}', [SengketaController::class, 'destroy'])->name('sengketa.destroy');
    Route::get('/users', [UserController::class, 'index'])->name('users');
    Route::get('/users/approve/{id}', [UserController::class, 'approve'])->name('user.approve');
    Route::get('/users/desmin/{id}', [UserController::class, 'desmin'])->name('user.desmin');
});


require __DIR__.'/auth.php';
