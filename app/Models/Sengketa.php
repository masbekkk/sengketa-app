<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sengketa extends Model
{
    use HasFactory;

    public function negara()
    {
        return $this->belongsTo(Negara::class, 'negara_id');
    }
}
