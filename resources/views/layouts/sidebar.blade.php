<div class="main-sidebar">
	<aside id="sidebar-wrapper">
		<div class="sidebar-brand my-3">
			<a href="/"><img src="{{ asset('img/app-logo.png') }}" alt="" class="sidebar-title"></a>
		</div>
		<div class="sidebar-brand sidebar-brand-sm">
			<a href="/"><img src="{{ asset('img/app-logo.png') }}" alt="NID" class="sidebar-title"></a>
		</div>
		<ul class="sidebar-menu">
			@if(Auth::check())
			@if(Auth::user()->status == 2)
			<li class="menu-header">Users</li>
			<li class="{{ Route::is('users') ? 'active' : '' }}"><a class="nav-link" href="{{ route('users') }}"><i class="fas fa-check-double"></i> <span>All Users</span></a></li>
			@endif
			@endif
			<li class="menu-header">Dashboard</li>
			<li class="{{ Route::is('sengketa.dashboard') ? 'active' : '' }}"><a class="nav-link" href="{{ route('sengketa.dashboard') }}"><i class="fas fa-columns"></i> <span>Dashboard</span></a></li>
			<li class="{{ Route::is('sengketa.index') ? 'active' : '' }}"><a class="nav-link" href="{{ route('sengketa.index') }}"><i class="fas fa-database"></i> <span>All Data</span></a></li>

			<li class="menu-header">Status</li>
			<li class="side"><a class="nav-link" href="{{ route('sengketa.show', ['sengketum' => 'open']) }}"><i class="fas fa-cog"></i> <span>Open Cases</span></a></li>
			<li class="side"><a class="nav-link" href="{{ route('sengketa.show', ['sengketum' => 'close']) }}"><i class="fas fa-wrench"></i> <span>Closed Cases</span></a></li>

			<li class="menu-header">Detail</li>
			<li class="side"><a class="nav-link" href="{{ route('sengketa.show', ['sengketum' => 'map']) }}"><i class="fas fa-tag"></i> <span>MAP Cases</span></a></li>
			<li class="side"><a class="nav-link" href="{{ route('sengketa.show', ['sengketum' => 'bapa']) }}"><i class="fas fa-tag"></i> <span>BAPA Cases</span></a></li>
			<li class="side"><a class="nav-link" href="{{ route('sengketa.show', ['sengketum' => 'uapa']) }}"><i class="fas fa-tag"></i> <span>UAPA Cases</span></a></li>
		</ul>
	</aside>
</div>