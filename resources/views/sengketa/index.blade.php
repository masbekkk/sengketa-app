@extends('layouts.form')

@section('style')
	<!-- Table Style -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
	{{-- Header Section --}}
   	<div class="section-header bg-white">
        <h1>All Data</h1>
		<div class="section-header-breadcrumb transparent">
			@guest
			@else
				<a href="{{ route('sengketa.create') }}" class="btn btn-primary btn-lg btn-icon-split btn-block">
					<div><i class="fas fa-plus"></i>Add Data</div>
				</a>
			@endguest
			
		</div>
    </div>
	{{-- End Header Section --}}

	{{-- Content Table Section --}}
	<div class="section-body">
		<div class="card bg-transparent neumorph text-dark" style="font-size: 1em !important;">
			<div class="card-header">
				<a href="{{route('sengketa.export')}}" class="btn btn-icon icon-left btn-primary"><i class="fas fa-download"></i> Export Data</a>
			</div>
			<div class="card-body pb-2">
				<div class="table-responsive">
					<table class="table table-striped bg-white w-100" id="table-1">
						<thead>                  
							<tr role="row">
								<th class="text-center" style="width: 25px;">No.</th>
								<th class="">Nama WP</th>
								<th class="">Pihak Afiliasi</th>
								<th class="">Tipe</th>
								<th class="">Negara</th>
								<th class="">Seksi</th>
								<th class="">PIC</th>
								<th class="">Starting date</th>
								<th class="">Milestone 1</th>
								<th class="">End Date</th>
								<th class="">Status</th>
								<th class="" style="width: 100px;">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data as $keys => $a)
							<tr role="row" class="odd">
								<td class="sorting_1 align-middle text-center">{{ ++$keys }}</td>
								<td class="align-middle">{{ $a->nama_npwp }}</td>
								<td class="align-middle">{{ $a->pihak_afiliasi }}</td>
								<td class="align-middle">{{ strtoupper($a->type_kasus) }}</td>
								<td class="align-middle">{{ $a->negara->name }}</td>
								<td class="align-middle">{{ $a->seksi}}</td>
								<td class="align-middle">{{ $a->pic }}</td>
								<td class="align-middle">{{date('d M Y', strtotime($a->starting_date))}}</td>
								<td class="align-middle">{{ $a->milestone1 }}</td>
								<td class="align-middle">{{date('d M Y', strtotime($a->end_date))}}</td>
								<td class="align-middle">
									@if($a->case_status === "open") Open
									@else Close @endif
								</td>
							
								<td class="d-flex justify-content-center">
									<div class="row w-100">
									@guest
									@else
										<div class="col-12 d-flex justify-content-between">
											<a class="btn btn-primary btn-sm text-white w-50 mr-1" href="{{ route('sengketa.edit', ['sengketum' => $a->id]) }}" title="Edit"><i class="fas fa-edit"></i></a>
											<a class="btn btn-danger btn-sm text-white w-50 ml-1" href="{{ route('sengketa.destroy', ['sengketum' => $a->id]) }}" onclick="confirm('Are you sure want delete this Data?'); event.preventDefault(); document.getElementById('delete-data-form').submit();"><i class="fas fa-trash"></i>
												<form action="{{ route('sengketa.destroy', ['sengketum' => $a->id]) }}" method="POST" class="d-none" id="delete-data-form">
													@csrf
													@method('DELETE')
												</form>  
											</a>
										</div>
										@endguest
										<div class="col-12 d-flex justify-content-center mt-2 w-100">
											<a class="btn btn-info w-50 mr-1 btn-sm text-white" href="{{route('sengketa.detail', ['id' => $a->id])}}" title="Detail">
												<i class="fas fa-circle-info"></i>
											</a>
										</div>

									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	{{-- End Content Table Section --}}
@endsection

@section('script')
	<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#table-1').DataTable();
		});
	</script>
@endsection
