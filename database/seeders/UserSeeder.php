<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
Use Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new User();
        $data->nip = '12345678';
        $data->name = 'admin';
        $data->password = Hash::make('password');
        $data->status = 2;
        $data->save();
    }
}
