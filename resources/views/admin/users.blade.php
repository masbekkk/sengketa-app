@extends('layouts.form')

@section('style')
	<!-- Table Style -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
	{{-- Header Section --}}
   	<div class="section-header bg-white">
        <h1>All Users</h1>
		<div class="section-header-breadcrumb transparent">
		</div>
    </div>
	{{-- End Header Section --}}

	{{-- Content Table Section --}}
	<div class="section-body">
		<div class="card bg-transparent neumorph text-dark" style="font-size: 1em !important;">
			<div class="card-body pb-2">
				<div class="table-responsive">
					<table class="table table-striped bg-white w-100" id="table-1">
						<thead>                  
							<tr role="row">
								<th class="text-center" style="width: 25px;">No.</th>
								<th class="">NIP</th>
								<th class="">Name</th>
                                <th class="">Tgl Register</th>
								<th class="" style="width: 100px;">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data as $keys => $a)
							<tr role="row" class="odd">
								<td class="sorting_1 align-middle text-center">{{ ++$keys }}</td>
								<td class="align-middle">{{ $a->nip }}</td>
								<td class="align-middle">{{ $a->name }}</td>
                                <td class="align-middle">{{date('d M Y', strtotime($a->created_at))}}</td>
								<td class="align-middle">
                                    @if($a->status == 0)
                                    <a class="btn btn-danger btn-lg" href="{{ route('user.approve', ['id' => $a->id]) }}" onclick="return confirm('Set user {{$a->name}} to Admin Privelege?');"><i class="fas fa-check"></i> Set Admin </a>
                                    @else
									<a class="btn btn-danger btn-lg" href="{{ route('user.desmin', ['id' => $a->id]) }}" onclick="return confirm('Delete Admin Privelege user {{$a->name}}?');"><i class="fas fa-check"></i> Delete Admin </a>
                                    @endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	{{-- End Content Table Section --}}
@endsection

@section('script')
	<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#table-1').DataTable();
		});
	</script>
@endsection
