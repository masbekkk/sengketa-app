# WEB MONITOR DATA SENGKETA BY MOHAMMAD SUBKHAN 
 
 an app for crud and monitoring data sengketa
 
 ## Feature
 - [x] Feedback with sweet alert
 - [x] Visualize Data Users, Approve Users
 - [x] Create, Read, Update and Delete Data Sengketa
 - [x] Filter Data Sengketa

 ## How to run project on localhost
 - [x] Clone this repository 
 - [x] composer install
 - [x] cp .env.example .env
 - [x] Create DB on mysql server. Name db is sengketa
 - [x] php artisan key:generate
 - [x] php artisan migrate:refresh --seed
 - [x] php artisan serve
 - [x] Your App is running

 ## Akun Admin
 - [x] UserID = 12345678
 - [x] Password = password
