<?php

namespace App\Http\Controllers;

use App\Models\Sengketa;
use App\Exports\SengketaExport;
use App\Models\User;
use App\Models\Negara;
use Illuminate\Http\Request;
Use Alert;
// use 
Use Auth;
use Carbon\Carbon;

class SengketaController extends Controller
{
    public function index()
    {
        $data = Sengketa::with('negara')->get();
        return view('sengketa.index', ['data' => $data]);
    }

    public function dashboard()
    {
        $open_map = Sengketa::where('type_kasus', 'map')->where('case_status', 'open')->count();
        $open_bapa = Sengketa::where('type_kasus', 'bapa')->where('case_status', 'open')->count();
        $open_uapa = Sengketa::where('type_kasus', 'uapa')->where('case_status', 'open')->count();
        $close_map = Sengketa::where('type_kasus', 'map')->where('case_status', 'close')->count();
        $close_bapa = Sengketa::where('type_kasus', 'bapa')->where('case_status', 'close')->count();
        $close_uapa = Sengketa::where('type_kasus', 'uapa')->where('case_status', 'close')->count();
        $close_case = Sengketa::where('case_status', 'close')->count();
        $seksi1 = Sengketa::where('seksi', 'PPSPI1')->where('case_status', 'open')->count();
        $seksi2 = Sengketa::where('seksi', 'PPSPI2')->where('case_status', 'open')->count();
        $seksi3 = Sengketa::where('seksi', 'PPSPI3')->where('case_status', 'open')->count();
        $seksi4 = Sengketa::where('seksi', 'PPSPI4')->where('case_status', 'open')->count();
        $data = array(
            0 => array(
                'Open MAP',
                 $open_map
            ),
            1 => array(
                'Open BAPA',
                 $open_bapa
            ),
            2 => array(
                'Open UAPA',
                 $open_uapa
            ),
            3 => array(
                'Closed Cases',
                 $close_case
            ),
        );

        return view('sengketa.dashboard', [
            'data' => $data,
            'seksi1' => $seksi1,
            'seksi2' => $seksi2,
            'seksi3' => $seksi3,
            'seksi4' => $seksi4,
            'open_map' => $open_map,
            'open_uapa' => $open_uapa,
            'open_bapa' => $open_bapa,
            'close_map' => $close_map,
            'close_uapa' => $close_uapa,
            'close_bapa' => $close_bapa,
        ]);
    }

    public function show($sengketa)
    {
        $data = Sengketa::where('type_kasus', $sengketa)->orWhere('case_status', $sengketa)->with('negara')->get();
        return view('sengketa.show', ['data' => $data, 'param' => $sengketa]);
    }
    public function detail($id)
    {
        $data = Sengketa::findOrFail($id);
        return view('sengketa.detail', ['data' => $data]);  
    }

    public function create()
    {
        if(Auth::user()->status == 0){
            return redirect()->back()->with('toast_error', 'Kamu tidak ada akses! <br> Hubungi SuperAdmin untuk dapat Akses!');
        }else{
            $user = User::all();
            $negara = Negara::orderBy('jml', 'DESC')->get();
            return view('sengketa.create', ['user' => $user, 'negara' => $negara]);
        } 
        
    }

    public function store(Request $request)
    {
        if(Auth::user()->status == 0){
            return redirect()->back()->with('toast_error', 'Kamu tidak ada akses! <br> Hubungi SuperAdmin untuk dapat Akses!');
        }else{
        $data = new Sengketa();
        $data->case_id = $request->case_id;
        $data->npwp = $request->npwp;
        $data->nama_npwp = $request->nama_npwp;
        $data->kpp_terdaftar = $request->kpp_terdaftar;
        $data->type_kasus = $request->type_kasus;
        $data->seksi = $request->seksi;
        $data->tgl_permohonan = $request->tgl_permohonan;
        $data->pihak_afiliasi = $request->pihak_afiliasi;
        $data->covered_term = $request->covered_term;
        $data->covered_transaction = $request->covered_transaction;
        $data->no_surat_tugas = $request->no_surat_tugas;
        $data->tgl_surat_tugas = $request->tgl_surat_tugas;
        if($request->pic != null)
        $data->pic = implode(',',$request->pic);
        $data->tgl_kkpt = $request->tgl_kkpt;
        $data->no_surat_pemberitahuan_wp = $request->no_surat_pemberitahuan_wp;
        $data->tgl_surat_pemberitahuan_wp = $request->tgl_surat_pemberitahuan_wp;
        $data->no_notification = $request->no_notification;
        $data->date_notification = $request->date_notification;
        $data->no_risalah_komhas = $request->no_risalah_komhas;
        $data->tgl_komhas = $request->tgl_komhas;
        $data->tgl_mutual_agreement = $request->tgl_mutual_agreement;
        $data->no_sk_map = $request->no_sk_map;
        $data->tgl_sk_map = $request->tgl_sk_map;
        $data->no_sk_pembetulan = $request->no_sk_pembetulan;
        $data->tgl_sk_pembetulan = $request->tgl_sk_pembetulan;
        $data->no_closing_letter_dgt = $request->no_closing_letter_dgt;
        $data->no_closing_letter_camitra = $request->no_closing_letter_camitra;
        $data->tgl_closing_letter_camitra = $request->tgl_closing_letter_camitra;
        $data->currency = $request->currency;
        $data->nilai_cfm_wp = $request->nilai_cfm_wp;
        $data->nilai_cfm_pemeriksa = $request->nilai_cfm_pemeriksa;
        $data->koreksi_primer = $request->koreksi_primer;
        $data->nilai_cfm_ma = $request->nilai_cfm_ma;
        $data->downward_adjusment = $request->downward_adjusment;
        $data->refund = $request->refund;
        $data->year_category = $request->year_category;
        $data->year_started = $request->year_started;
        $data->conclude_year = $request->conclude_year;
        $data->ended_year = $request->ended_year;
        $data->starting_date = $request->starting_date;
        $data->milestone1 = $request->milestone1;
        $data->end_date = $request->end_date;
        $data->outcome = $request->outcome;
        $data->case_status = $request->case_status;
        $string = rand(5, 9099);
        $path = 'files';

        $file_surat_permohonan = $request->file('file_surat_permohonan');
        if($file_surat_permohonan != null){
            $fileName = $string . '__file_surat_permohonan__.' . $file_surat_permohonan->getClientOriginalExtension();
            $file_surat_permohonan->move($path, $fileName);
            $data->file_surat_permohonan = $path . '/' . $fileName;
        }

        $file_surat_tugas = $request->file('file_surat_tugas');
        if($file_surat_tugas != null){
            $fileName = $string . '__file_surat_tugas__.' . $file_surat_tugas->getClientOriginalExtension();
            $file_surat_tugas->move($path, $fileName);
            $data->file_surat_tugas = $path . '/' . $fileName;
        }

        $file_kkpt_formal = $request->file('file_kkpt_formal');
        if($file_kkpt_formal != null){
            $fileName = $string . '__file_kkpt_formal__.' . $file_kkpt_formal->getClientOriginalExtension();
            $file_kkpt_formal->move($path, $fileName);
            $data->file_kkpt_formal = $path . '/' . $fileName;
        }

        $file_notification = $request->file('file_notification');
        if($file_notification != null){
            $fileName = $string . '__file_notification__.' . $file_notification->getClientOriginalExtension();
            $file_notification->move($path, $fileName);
            $data->file_notification = $path . '/' . $fileName;
        }

        $file_risalah_komhas = $request->file('file_risalah_komhas');
        if($file_risalah_komhas != null){
            $fileName = $string . '__file_risalah_komhas__.' . $file_risalah_komhas->getClientOriginalExtension();
            $file_risalah_komhas->move($path, $fileName);
            $data->file_risalah_komhas = $path . '/' . $fileName;
        }

        $file_position_paper = $request->file('file_position_paper');
        if($file_position_paper != null){
            $fileName = $string . '__file_position_paper__.' . $file_position_paper->getClientOriginalExtension();
            $file_position_paper->move($path, $fileName);
            $data->file_position_paper = $path . '/' . $fileName;
        }

        $file_laporan_penelaahan = $request->file('file_laporan_penelaahan');
        if($file_laporan_penelaahan != null){
            $fileName = $string . '__file_laporan_penelaahan__.' . $file_laporan_penelaahan->getClientOriginalExtension();
            $file_laporan_penelaahan->move($path, $fileName);
            $data->file_laporan_penelaahan = $path . '/' . $fileName;
        }

        $file_mutual_agreement = $request->file('file_mutual_agreement');
        if($file_mutual_agreement != null){
            $fileName = $string . '__file_mutual_agreement__.' . $file_mutual_agreement->getClientOriginalExtension();
            $file_mutual_agreement->move($path, $fileName);
            $data->file_mutual_agreement = $path . '/' . $fileName;
        }

        $file_upload_sk_map = $request->file('file_upload_sk_map');
        if($file_upload_sk_map != null){
            $fileName = $string . '__file_upload_sk_map__.' . $file_upload_sk_map->getClientOriginalExtension();
            $file_upload_sk_map->move($path, $fileName);
            $data->file_upload_sk_map = $path . '/' . $fileName;
        }

        $file_sk_pembetulan = $request->file('file_sk_pembetulan');
        if($file_sk_pembetulan != null){
            $fileName = $string . '__file_sk_pembetulan__.' . $file_sk_pembetulan->getClientOriginalExtension();
            $file_sk_pembetulan->move($path, $fileName);
            $data->file_sk_pembetulan = $path . '/' . $fileName;
        }
        $cek_negara = Negara::find($request->negara_id);
        if(!empty($cek_negara)){
            $data->negara_id = $request->negara_id;
            $cek_negara->jml = $cek_negara->jml+1;
            $cek_negara->save();
        }else{
            $negara = new Negara();
            $negara->name = $request->negara;
            $negara->jml = 1;
            $negara->save();
            $data->negara_id = $negara->id;
        }

        $data->save();
        return redirect()->route('sengketa.index')
        ->with('toast_success', 'Data berhasil ditambahkan!');
    }

    }

    public function edit($sengketa)
    {
        if(Auth::user()->status == 0){
            return redirect()->back()->with('toast_error', 'Kamu tidak ada akses! <br> Hubungi SuperAdmin untuk dapat Akses!');
        }else{
            $data = Sengketa::findOrFail($sengketa);
            $user = User::all();
            $negara = Negara::orderBy('jml', 'DESC')->get();
            return view('sengketa.edit', ['user' => $user, 'data' => $data, 'negara' => $negara]);
        }
    }

    public function update(Request $request, $sengketa)
    {
        if(Auth::user()->status == 0){
            return redirect()->back()->with('toast_error', 'Kamu tidak ada akses! <br> Hubungi SuperAdmin untuk dapat Akses!');
        }else{
        $data = Sengketa::findOrFail($sengketa);
        $data->case_id = $request->case_id;
        $data->npwp = $request->npwp;
        $data->nama_npwp = $request->nama_npwp;
        $data->kpp_terdaftar = $request->kpp_terdaftar;
        $data->type_kasus = $request->type_kasus;
        $data->seksi = $request->seksi;
        $data->tgl_permohonan = $request->tgl_permohonan;
        $data->pihak_afiliasi = $request->pihak_afiliasi;
        $data->covered_term = $request->covered_term;
        $data->covered_transaction = $request->covered_transaction;
        $data->no_surat_tugas = $request->no_surat_tugas;
        $data->tgl_surat_tugas = $request->tgl_surat_tugas;
        if($request->pic != null)
        $data->pic = implode(',',$request->pic);
        $data->tgl_kkpt = $request->tgl_kkpt;
        $data->no_surat_pemberitahuan_wp = $request->no_surat_pemberitahuan_wp;
        $data->tgl_surat_pemberitahuan_wp = $request->tgl_surat_pemberitahuan_wp;
        $data->no_notification = $request->no_notification;
        $data->date_notification = $request->date_notification;
        $data->no_risalah_komhas = $request->no_risalah_komhas;
        $data->tgl_komhas = $request->tgl_komhas;
        
        $data->tgl_mutual_agreement = $request->tgl_mutual_agreement;
        $data->no_sk_map = $request->no_sk_map;
        $data->tgl_sk_map = $request->tgl_sk_map;
        $data->no_sk_pembetulan = $request->no_sk_pembetulan;
        $data->tgl_sk_pembetulan = $request->tgl_sk_pembetulan;
        $data->no_closing_letter_dgt = $request->no_closing_letter_dgt;
        $data->no_closing_letter_camitra = $request->no_closing_letter_camitra;
        $data->tgl_closing_letter_camitra = $request->tgl_closing_letter_camitra;
        $data->currency = $request->currency;
        $data->nilai_cfm_wp = $request->nilai_cfm_wp;
        $data->nilai_cfm_pemeriksa = $request->nilai_cfm_pemeriksa;
        $data->koreksi_primer = $request->koreksi_primer;
        $data->nilai_cfm_ma = $request->nilai_cfm_ma;
        $data->downward_adjusment = $request->downward_adjusment;
        $data->refund = $request->refund;
        $data->oecd_category = $request->oecd_category;
        $data->year_category = $request->year_category;
        $data->year_started = $request->year_started;
        $data->conclude_year = $request->conclude_year;
        $data->ended_year = $request->ended_year;
        $data->starting_date = $request->starting_date;
        $data->milestone1 = $request->milestone1;
        $data->end_date = $request->end_date;
        $data->outcome = $request->outcome;
        $data->case_status = $request->case_status;
        $string = rand(5, 9099);
        $path = 'files';

        $file_surat_permohonan = $request->file('file_surat_permohonan');
        if($file_surat_permohonan != null){
            $fileName = $string . '__file_surat_permohonan__.' . $file_surat_permohonan->getClientOriginalExtension();
            $file_surat_permohonan->move($path, $fileName);
            $data->file_surat_permohonan = $path . '/' . $fileName;
        }

        $file_surat_tugas = $request->file('file_surat_tugas');
        if($file_surat_tugas != null){
            $fileName = $string . '__file_surat_tugas__.' . $file_surat_tugas->getClientOriginalExtension();
            $file_surat_tugas->move($path, $fileName);
            $data->file_surat_tugas = $path . '/' . $fileName;
        }

        $file_kkpt_formal = $request->file('file_kkpt_formal');
        if($file_kkpt_formal != null){
            $fileName = $string . '__file_kkpt_formal__.' . $file_kkpt_formal->getClientOriginalExtension();
            $file_kkpt_formal->move($path, $fileName);
            $data->file_kkpt_formal = $path . '/' . $fileName;
        }

        $file_notification = $request->file('file_notification');
        if($file_notification != null){
            $fileName = $string . '__file_notification__.' . $file_notification->getClientOriginalExtension();
            $file_notification->move($path, $fileName);
            $data->file_notification = $path . '/' . $fileName;
        }

        $file_risalah_komhas = $request->file('file_risalah_komhas');
        if($file_risalah_komhas != null){
            $fileName = $string . '__file_risalah_komhas__.' . $file_risalah_komhas->getClientOriginalExtension();
            $file_risalah_komhas->move($path, $fileName);
            $data->file_risalah_komhas = $path . '/' . $fileName;
        }

        $file_position_paper = $request->file('file_position_paper');
        if($file_position_paper != null){
            $fileName = $string . '__file_position_paper__.' . $file_position_paper->getClientOriginalExtension();
            $file_position_paper->move($path, $fileName);
            $data->file_position_paper = $path . '/' . $fileName;
        }

        $file_laporan_penelaahan = $request->file('file_laporan_penelaahan');
        if($file_laporan_penelaahan != null){
            $fileName = $string . '__file_laporan_penelaahan__.' . $file_laporan_penelaahan->getClientOriginalExtension();
            $file_laporan_penelaahan->move($path, $fileName);
            $data->file_laporan_penelaahan = $path . '/' . $fileName;
        }

        $file_mutual_agreement = $request->file('file_mutual_agreement');
        if($file_mutual_agreement != null){
            $fileName = $string . '__file_mutual_agreement__.' . $file_mutual_agreement->getClientOriginalExtension();
            $file_mutual_agreement->move($path, $fileName);
            $data->file_mutual_agreement = $path . '/' . $fileName;
        }

        $file_upload_sk_map = $request->file('file_upload_sk_map');
        if($file_upload_sk_map != null){
            $fileName = $string . '__file_upload_sk_map__.' . $file_upload_sk_map->getClientOriginalExtension();
            $file_upload_sk_map->move($path, $fileName);
            $data->file_upload_sk_map = $path . '/' . $fileName;
        }

        $file_sk_pembetulan = $request->file('file_sk_pembetulan');
        if($file_sk_pembetulan != null){
            $fileName = $string . '__file_sk_pembetulan__.' . $file_sk_pembetulan->getClientOriginalExtension();
            $file_sk_pembetulan->move($path, $fileName);
            $data->file_sk_pembetulan = $path . '/' . $fileName;
        }
        $cek_negara = Negara::find($request->negara_id);
        if(!empty($cek_negara)){
            $data->negara_id = $request->negara_id;
            $cek_negara->jml = $cek_negara->jml;
            $cek_negara->save();
        }else{
            $negara = new Negara();
            $negara->name = $request->negara;
            $negara->jml = 1;
            $negara->save();
            $data->negara_id = $negara->id;
        }

        $data->save();
        return redirect()->route('sengketa.index')
        ->with('toast_success', 'Data berhasil diupdate!');
    }
    }

    public function destroy($sengketa)
    {
        if(Auth::user()->status == 0){
            return redirect()->back()->with('toast_error', 'Kamu tidak ada akses! <br> Hubungi SuperAdmin untuk dapat Akses!');
        }else{
        $data = Sengketa::findOrFail($sengketa)->delete();
        return redirect()->route('sengketa.index')
        ->with('toast_success', 'Data berhasil dihapus!');
        }
    }

    public function export()
    {
        return new SengketaExport(Carbon::parse(Carbon::now())->toFormattedDateString());
    }
}
