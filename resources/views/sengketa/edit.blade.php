@extends('layouts.form')

@section('style')
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
	{{-- Header Section --}}
	<div class="section-header">
		<h1>Edit Data</h1>
	</div>
	{{-- End Header Section --}}
	
	{{-- Content Form Section --}}
	<div class="section-body">
		<form action="{{route('sengketa.update', ['sengketum' => $data->id])}}" method="POST" enctype="multipart/form-data">
			@csrf
			{{method_field('PUT')}}
			@section('modal')
			<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Primary Adjustment</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body">
					<div class="card-body pb-0">
						<div class="row">
							<div class="col-md">
								<label for="currency" class="col-form-label">Currency</label>
								<select class="form-control" id="currency" name="currency" value="{{$data->currency}}">
									<option selected value="{{$data->currency}}">{{$data->currency}}</option>
									<option value="IDR">IDR</option>
									<option value="USD">USD</option>
								</select>
							</div>
							<div class="col-md">
								<label for="nilai_cfm_wp" class="col-form-label">Nilai cfm WP</label>
								<input type="number" class="form-control" id="nilai_cfm_wp" name="nilai_cfm_wp" value="{{ $data->nilai_cfm_wp}}">
							</div>
						</div>
						<div class="row">
							<div class="col-md">
								<label for="nilai_cfm_pemeriksa" class="col-form-label">Nilai cfm Pemeriksa</label>
								<input type="number" class="form-control" id="nilai_cfm_pemeriksa" name="nilai_cfm_pemeriksa" value="{{ $data->nilai_cfm_pemeriksa}}">      
							</div>
							<div class="col-md">
								<label for="koreksi_primer" class="col-form-label">Koreksi Primer</label>
								<input type="number" class="form-control" id="koreksi_primer" name="koreksi_primer" value="{{ $data->koreksi_primer}}">
							</div>
						</div>
						<div class="row">
							<div class="col-md">
								<label for="nilai_cfm_ma" class="col-form-label">Nilai cfm MA</label>
								<input type="number" class="form-control" id="nilai_cfm_ma" name="nilai_cfm_ma" value="{{ $data->nilai_cfm_ma}}">      
							</div>
							<div class="col-md">
								<label for="downward_adjusment" class="col-form-label">Downward Adjustment</label>
								<input type="number" class="form-control" id="downward_adjusment" name="downward_adjusment" value="{{ $data->downward_adjusment}}">
							</div>
						</div>
						<div class="row">
							<div class="col-md">
								<label for="refund" class="col-form-label">Refund</label>
								<input type="number" class="form-control" id="refund" name="refund" value="{{ $data->refund}}">
							</div>
						</div>
					</div>
					</div>
					<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</div>
				</div>
			</div>
			@endsection
			<div class="card bg-transparent neumorph">
				<div class="card-body pb-0">
					<h2 class="section-title">Informasi Kasus</h2>
					<div class="row">
						<div class="col-md">
							<input type="hidden" id="form_currency" name="currency" value="{{ $data->currency}}">
							<input type="hidden" id="form_nilai_cfm_wp" name="nilai_cfm_wp" value="{{ $data->nilai_cfm_wp}}">
							<input type="hidden" id="form_nilai_cfm_pemeriksa" name="nilai_cfm_pemeriksa" value="{{ $data->nilai_cfm_pemeriksa}}">
							<input type="hidden" id="form_koreksi_primer" name="koreksi_primer" value="{{ $data->koreksi_primer}}">
							<input type="hidden" id="form_nilai_cfm_ma" name="nilai_cfm_ma" value="{{ $data->nilai_cfm_ma}}">
							<input type="hidden" id="form_downward_adjusment" name="downward_adjusment" value="{{ $data->downward_adjusment}}">
							<input type="hidden" id="form_refund" name="refund" value="{{ $data->refund}}">
							<label for="case_id" class="col-form-label">Case ID</label>
							<input type="text" class="form-control" id="case_id" name="case_id" value="{{ $data->case_id}}">
						</div>
						<div class="col-md">
							<label for="npwp" class="col-form-label">NPWP</label>
							<input type="text" class="form-control" id="npwp" name="npwp" value="{{ $data->npwp}}">
						</div>
					</div>
					<div class="row">
						<div class="col-md">
							<label for="nama_npwp" class="col-form-label">Nama WP</label>
							<input type="text" class="form-control" id="nama_npwp" name="nama_npwp" value="{{ $data->nama_npwp}}">      
						</div>
						<div class="col-md">
							<label for="kpp_terdaftar" class="col-form-label">KPP Terdaftar</label>
							<input type="text" class="form-control" id="kpp_terdaftar" name="kpp_terdaftar" value="{{ $data->kpp_terdaftar}}">
						</div>
					</div>
					<div class="row">
						<div class="col-md">
							<label for="type_kasus" class="col-form-label">Tipe Kasus</label>
							<select class="form-control" id="type_kasus" name="type_kasus">
								<option selected value="{{$data->type_kasus}}">{{strtoupper($data->type_kasus)}}</option>
								<option value="map">MAP</option>
								<option value="bapa">BAPA</option>
								<option value="uapa">UAPA</option>
							</select>
						</div>
						<div class="col-md">
							<label for="seksi" class="col-form-label">Seksi</label>
							<select class="form-control" id="seksi" name="seksi">
								<option selected value="{{$data->seksi}}">{{$data->seksi}}</option>
								<option value="PPSPI1">PPSPI1</option>
								<option value="PPSPI2">PPSPI2</option>
								<option value="PPSPI3">PPSPI3</option>
								<option value="PPSPI4">PPSPI4</option>
							</select>
						</div>
						<div class="col-md">
							<label for="tgl_permohonan" class="col-form-label">Tanggal Permohonan</label>
							<input type="date" class="form-control" id="tgl_permohonan" name="tgl_permohonan" value="{{ $data->tgl_permohonan}}">
						</div>
					</div>
					<div class="row">
						<div class="col-md">
							<label for="pihak_afiliasi" class="col-form-label">Pihak Afiliasi</label>
							<input type="text" class="form-control" id="pihak_afiliasi" name="pihak_afiliasi" value="{{ $data->pihak_afiliasi}}">
						</div>
						<div class="col-md">
							<label for="negara_id" class="col-form-label">Negara/ Yurisdiksi</label>
							<select class="form-control select2" id="negara_id" name="negara_id">
								@foreach($negara as $item)
									@if($data->negara_id == $item->id)
									<option selected value={{$item->id}}>{{$item->name}} ({{$item->jml}})</option>
									@else
									<option value={{$item->id}}>{{$item->name}} ({{$item->jml}})</option>
									@endif
								@endforeach
							</select>
							<input type="hidden" id="form_negara" name="negara" value="{{ old('negara') }}">
						</div>
						<div class="col-md">
							<label for="covered_term" class="col-form-label">Covered Term</label>
							<input type="text" class="form-control" id="covered_term" name="covered_term" value="{{ $data->covered_term}}">
						</div>
					
					</div>
					<div class="row">
						<div class="col-md">
							<label for="covered_transaction" class="col-form-label">Covered Transaction</label>
							<input type="text" class="form-control" id="covered_transaction" name="covered_transaction" value="{{ $data->covered_transaction}}">
						</div>
						<div class="col-md">
							<label for="file_surat_permohonan" class="col-form-label">File Surat Permohonan</label>
							&nbsp;&nbsp;
							<text class="font-weight-bold text-lg">
								<i class="fas fa-file"></i>
								<a href="{{ asset($data->file_surat_permohonan)}}" target="_blank" class="text-danger"> File Sebelumnya</a>
							</text>
							<input type="file" class="form-control" id="file_surat_permohonan" name="file_surat_permohonan" value="{{ $data->file_surat_permohonan}}">
						</div>
					</div>
					<br>
					<div class="row" id="buttonSection">
						<div class="col-md-3">
							<label class="col-form-label">Penelitian Formal</label>
							<br>
							<a href="#penelitianFormal" type="button" class="btn btn-light btn-lg w-100" data-toggle="collapse" data-target="#sectionPenelitianFormal" aria-expanded="false" aria-controls="collapseExample"><i class="fas fa-file-pen"></i> Penelitian Formal</a>
						</div>
						<div class="col-md-3">
							<label class="col-form-label">Penelitian Material</label>
							<br>
							<a href="#penelitianMaterial" class="btn btn-secondary btn-lg w-100" data-toggle="collapse" data-target="#sectionPenelitianMaterial" aria-expanded="false" aria-controls="collapseExample"><i class="fas fa-file-pen"></i> Penelitian Material</a>
						</div>
						<div class="col-md-3">
							<label class="col-form-label">Penyelesaian</label>
							<br>
							<a href="#penyelesaian" class="btn btn-light btn-lg w-100" data-toggle="collapse" data-target="#sectionPenyelesaian" aria-expanded="false" aria-controls="collapseExample"><i class="fas fa-file-pen"></i> Penyelesaian</a>
						</div>
						<div class="col-md-3">
							<label class="col-form-label">OECD Statistics</label>
							<br>
							<a href="#oecd" class="btn btn-secondary btn-lg w-100" data-toggle="collapse" data-target="#sectionOECDStatistics" aria-expanded="false" aria-controls="collapseExample"><i class="fas fa-file-pen"></i> OECD Statistics</a>
						</div>
					</div> 
					<div data-spy="scroll" data-target="#buttonSection" data-offset="0">
					<div class="collapse" id="sectionPenelitianFormal">
						<h2 class="section-title" id="penelitianFormal">Penelitian Formal</h2>
						<div class="row">
							<div class="col-md">
								<label for="no_surat_tugas" class="col-form-label">No. Surat Tugas</label>
								<input type="text" class="form-control" id="no_surat_tugas" name="no_surat_tugas" value="{{ $data->no_surat_tugas}}">
							</div>
							<div class="col-md">
								<label for="tgl_surat_tugas" class="col-form-label">Tanggal Surat Tugas</label>
								<input type="date" class="form-control" id="tgl_surat_tugas" name="tgl_surat_tugas" value="{{ $data->tgl_surat_tugas}}">
							</div>
						</div>
						<div class="row">
							<div class="col-md">
								<label for="pic" class="col-form-label">PIC</label>
								<select class="form-control select2" id="pic" name="pic[]" multiple="multiple" value="{{$data->pic}}">
									<option selected value="{{$data->pic}}">{{$data->pic}}</option>
									@foreach ($user as $item)
										<option value="{{$item->name}}">{{$item->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md">
								<label for="tgl_kkpt" class="col-form-label">Tanggal KKPt Formal</label>
								<input type="date" class="form-control" id="tgl_kkpt" name="tgl_kkpt" value="{{ $data->tgl_kkpt}}">
							</div>
							<div class="col-md">
								<label for="no_surat_pemberitahuan_wp" class="col-form-label">No. Surat Pemberitahuan ke WP</label>
								<input type="text" class="form-control" id="no_surat_pemberitahuan_wp" name="no_surat_pemberitahuan_wp" value="{{ $data->no_surat_pemberitahuan_wp}}">
							</div>
							<div class="col-md">
								<label for="tgl_surat_pemberitahuan_wp" class="col-form-label">Tgl Surat Pemberitahuan ke WP</label>
								<input type="text" class="form-control" id="tgl_surat_pemberitahuan_wp" name="tgl_surat_pemberitahuan_wp" value="{{ $data->tgl_surat_pemberitahuan_wp }}">
							</div>
						</div>
						<div class="row">
							<div class="col-md">
								<label for="no_notification" class="col-form-label">No. Notification/ Acknowledgement Letter</label>
								<input type="text" class="form-control" id="no_notification" name="no_notification" value="{{ $data->no_notification}}">
							</div>
							<div class="col-md">
								<label for="date_notification" class="col-form-label">Tanggal Notification/ Acknowledgement Letter</label>
								<input type="date" class="form-control" id="date_notification" name="date_notification" value="{{ $data->date_notification}}">
							</div>
						
						</div>
						<div class="row">
							<div class="col-md">
								<label for="file_surat_tugas" class="col-form-label">Upload Surat Tugas</label>
								&nbsp;&nbsp;
								<text class="font-weight-bold text-lg">
									<i class="fas fa-file"></i>
									<a href="{{ asset($data->file_surat_tugas)}}" target="_blank" class="text-danger"> File Sebelumnya</a>
								</text>
								<input type="file" class="form-control" id="file_surat_tugas" name="file_surat_tugas" value="{{ $data->file_surat_tugas}}">
							</div>
							<div class="col-md">
								<label for="file_kkpt_formal" class="col-form-label">Upload File KKPt Formal</label>
								&nbsp;&nbsp;
								<text class="font-weight-bold text-lg">
									<i class="fas fa-file"></i>
									<a href="{{ asset($data->file_kkpt_formal)}}" target="_blank" class="text-danger"> File Sebelumnya</a>
								</text>
								<input type="file" class="form-control" id="file_kkpt_formal" name="file_kkpt_formal" value="{{ $data->file_kkpt_formal}}">
							</div>
						</div>
						<div class="row">
							<div class="col-md">
								<label for="file_surat_pemberitahuan" class="col-form-label">Upload Surat Pemberitahuan</label>
								&nbsp;&nbsp;
								<text class="font-weight-bold text-lg">
									<i class="fas fa-file"></i>
									<a href="{{ asset($data->file_surat_pemberitahuan)}}" target="_blank" class="text-danger"> File Sebelumnya</a>
								</text>
								<input type="file" class="form-control" id="file_surat_pemberitahuan" name="file_surat_pemberitahuan" value="{{ $data->file_surat_pemberitahuan}}">
							</div>
							<div class="col-md">
								<label for="file_notification" class="col-form-label">Upload File Notification/ Acknowledgement Letter</label>
								&nbsp;&nbsp;
								<text class="font-weight-bold text-lg">
									<i class="fas fa-file"></i>
									<a href="{{ asset($data->file_notification)}}" target="_blank" class="text-danger"> File Sebelumnya</a>
								</text>
								<input type="file" class="form-control" id="file_notification" name="file_notification" value="{{ $data->file_notification}}">
							</div>
						</div>
					</div>
						<div class="collapse" id="sectionPenelitianMaterial">
							<h2 class="section-title" id="penelitianMaterial">Penelitian Material</h2>
							<div class="row">
								<div class="col-md">
									<label for="no_risalah_komhas" class="col-form-label">No. Risalah Komhas</label>
									<input type="text" class="form-control" id="no_risalah_komhas" name="no_risalah_komhas" value="{{ $data->no_risalah_komhas}}">
								</div>
								<div class="col-md">
									<label for="tgl_komhas" class="col-form-label">Tanggal Komhas</label>
									<input type="text" class="form-control" id="tgl_komhas" name="tgl_komhas" value="{{ $data->tgl_komhas}}">
								</div>
							</div>
							<div class="row">
								<div class="col-md">
									<label for="tgl_position_paper" class="col-form-label">Tanggal Position Paper</label>
									<input type="date" class="form-control" id="tgl_position_paper" name="tgl_position_paper" value="{{ $data->tgl_position_paper}}">      
								</div>
								<div class="col-md">
									<label for="no_laporan_penelaahan" class="col-form-label">No. Inputan Penelaahan</label>
									<input type="text" class="form-control" id="no_laporan_penelaahan" name="no_laporan_penelaahan" value="{{ $data->no_laporan_penelaahan}}">
								</div>
								<div class="col-md">
									<label for="tgl_laporan_penelaahan" class="col-form-label">Tanggal Laporan Penelaahan</label>
									<input type="date" class="form-control" id="tgl_laporan_penelaahan" name="tgl_laporan_penelaahan" value="{{ $data->tgl_laporan_penelaahan}}">
								</div>
							</div>
							<div class="row">
								<div class="col-md">
									<label for="file_risalah_komhas" class="col-form-label">Upload Risalah Komhas</label>
									&nbsp;&nbsp;
									<text class="font-weight-bold text-lg">
										<i class="fas fa-file"></i>
										<a href="{{ asset($data->file_risalah_komhas)}}" target="_blank" class="text-danger"> File Sebelumnya</a>
									</text>
									<input type="file" class="form-control" id="file_risalah_komhas" name="file_risalah_komhas" value="{{ $data->file_risalah_komhas}}">      
								</div>
								<div class="col-md">
									<label for="file_position_paper" class="col-form-label">Upload Position Paper</label>
									&nbsp;&nbsp;
									<text class="font-weight-bold text-lg">
										<i class="fas fa-file"></i>
										<a href="{{ asset($data->file_position_paper)}}" target="_blank" class="text-danger"> File Sebelumnya</a>
									</text>
									<input type="file" class="form-control" id="file_position_paper" name="file_position_paper" value="{{ $data->file_position_paper}}">
								</div>
								<div class="col-md">
									<label for="file_laporan_penelaahan" class="col-form-label">Upload Laporan Penelaahan</label>
									&nbsp;&nbsp;
									<text class="font-weight-bold text-lg">
										<i class="fas fa-file"></i>
										<a href="{{ asset($data->file_laporan_penelaahan)}}" target="_blank" class="text-danger"> File Sebelumnya</a>
									</text>
									<input type="file" class="form-control" id="file_laporan_penelaahan" name="file_laporan_penelaahan" value="{{ $data->file_laporan_penelaahan}}">
								</div>
							</div>
						</div>
						<div class="collapse" id="sectionPenyelesaian">
							<h2 class="section-title" id="penyelesaian">Penyelesaian</h2>
							<div class="row">
								<div class="col-md">
									<label for="tgl_mutual_agreement" class="col-form-label">Tanggal Mutual Agreement/ Naskah UAPA</label>
									<input type="date" class="form-control" id="tgl_mutual_agreement" name="tgl_mutual_agreement" value="{{ $data->tgl_mutual_agreement}}">
								</div>
								<div class="col-md">
									<label for="no_sk_map" class="col-form-label">No. SK MAP/ APA</label>
									<input type="text" class="form-control" id="no_sk_map" name="no_sk_map" value="{{ $data->no_sk_map}}">
								</div>
							</div>
							<div class="row">
								<div class="col-md">
									<label for="tgl_skp_map" class="col-form-label">Tanggal SK MAP/ APA</label>
									<input type="text" class="form-control" id="tgl_skp_map" name="tgl_skp_map" value="{{ $data->tgl_skp_map}}">      
								</div>
								<div class="col-md">
									<label for="no_sk_pembetulan" class="col-form-label">No. SK Pembetulan</label>
									<input type="text" class="form-control" id="no_sk_pembetulan" name="no_sk_pembetulan" value="{{ $data->no_sk_pembetulan}}">
								</div>
							</div>
							<div class="row">
								<div class="col-md">
									<label for="no_closing_letter_dgt" class="col-form-label">No. Closing Letter DGT</label>
									<input type="text" class="form-control" id="no_closing_letter_dgt" name="no_closing_letter_dgt" value="{{ $data->no_closing_letter_dgt}}">      
								</div>
								<div class="col-md">
									<label for="tgl_closing_letter_dgt" class="col-form-label">Tanggal Closing Letter DGT</label>
									<input type="text" class="form-control" id="tgl_closing_letter_dgt" name="tgl_closing_letter_dgt" value="{{ $data->tgl_closing_letter_dgt}}">
								</div>
							</div>
							<div class="row">
								<div class="col-md">
									<label for="no_closing_letter_camitra" class="col-form-label">No. Closing Letter Camitra</label>
									<input type="text" class="form-control" id="no_closing_letter_camitra" name="no_closing_letter_camitra" value="{{ $data->no_closing_letter_camitra}}">      
								</div>
								<div class="col-md">
									<label for="tgl_closing_letter_camitra" class="col-form-label">Tanggal Closing Letter Camitra</label>
									<input type="date" class="form-control" id="tgl_closing_letter_camitra" name="tgl_closing_letter_camitra" value="{{ $data->tgl_closing_letter_camitra}}">
								</div>
								<div class="col-md">
									<label for="primary_adjustment" class="col-form-label">Primary Adjustment</label>
									<br>
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
										Primary Adjustment
									</button>
								</div>
							</div>
							<div class="row">
								<div class="col-md">
									<label for="file_mutual_agreement" class="col-form-label">Upload Mutual Agreement/ Naskah UAPA</label>
									&nbsp;&nbsp;
									<text class="font-weight-bold text-lg">
										<i class="fas fa-file"></i>
										<a href="{{ asset($data->file_mutual_agreement)}}" target="_blank" class="text-danger"> File Sebelumnya</a>
									</text>
									<input type="file" class="form-control" id="file_mutual_agreement" name="file_mutual_agreement" value="{{ $data->file_mutual_agreement}}">      
								</div>
								<div class="col-md">
									<label for="file_upload_sk_map" class="col-form-label">Upload SK MAP/ APA</label>
									&nbsp;&nbsp;
									<text class="font-weight-bold text-lg">
										<i class="fas fa-file"></i>
										<a href="{{ asset($data->file_upload_sk_map)}}" target="_blank" class="text-danger"> File Sebelumnya</a>
									</text>
									<input type="file" class="form-control" id="file_upload_sk_map" name="file_upload_sk_map" value="{{ $data->file_upload_sk_map}}">
								</div>
								<div class="col-md">
									<label for="file_sk_pembetulan" class="col-form-label">Upload SK Pembetulan</label>
									&nbsp;&nbsp;
									<text class="font-weight-bold text-lg">
										<i class="fas fa-file"></i>
										<a href="{{ asset($data->file_sk_pembetulan)}}" target="_blank" class="text-danger"> File Sebelumnya</a>
									</text>
									<input type="file" class="form-control" id="file_sk_pembetulan" name="file_sk_pembetulan" value="{{ $data->file_sk_pembetulan}}">
								</div>
							</div>
						</div>
						<div class="collapse" id="sectionOECDStatistics">
							<h2 class="section-title" id="oecd">OECD Statistics</h2>
							<div class="row">
								<div class="col-md">
									<label for="oecd_category" class="col-form-label">OECD Category</label>
									<select class="form-control" id="oecd_category" name="oecd_category">
										<option selected value="{{$data->oecd_category}}">{{$data->oecd_category}}</option>
										<option value="Attribution">Attribution</option>
										<option value="Other">Other</option>
									</select>
								</div>
								<div class="col-md">
									<label for="year_category" class="col-form-label">Year Category</label>
									<input type="number" class="form-control" id="year_category" name="year_category" value="{{ $data->year_category}}">
								</div>
							</div>
							<div class="row">
								<div class="col-md">
									<label for="year_started" class="col-form-label">Year Started</label>
									<input type="number" class="form-control" id="year_started" name="year_started" value="{{ $data->year_started }}">
								</div>
								<div class="col-md">
									<label for="conclude_year" class="col-form-label">Conclude Year</label>
									<input type="number" class="form-control" id="conclude_year" name="conclude_year" value="{{ $data->conclude_year }}">
								</div>
								<div class="col-md">
									<label for="ended_year" class="col-form-label">Ended Year</label>
									<input type="number" class="form-control" id="ended_year" name="ended_year" value="{{ $data->ended_year }}">
								</div>
							</div>
							<div class="row">
								<div class="col-md">
									<label for="starting_date" class="col-form-label">Starting Date</label>
									<input type="date" class="form-control" id="starting_date" name="starting_date" value="{{ $data->starting_date}}">      
								</div>
								<div class="col-md">
									<label for="milestone1" class="col-form-label">Milestone 1</label>
									<input type="date" class="form-control" id="milestone1" name="milestone1" value="{{ $data->milestone1}}">
								</div>
								<div class="col-md">
									<label for="end_date" class="col-form-label">End Date</label>
									<input type="date" class="form-control" id="end_date" name="end_date" value="{{ $data->end_date}}">
								</div>
							</div>
							<div class="row">
								<div class="col-md">
									<label for="outcome" class="col-form-label">Outcome</label>
									<select class="form-control select2" id="outcome" name="outcome">
										<option selected value="{{$data->outcome}}">{{$data->outcome}}</option>
										<option value="denied MAP Access">denied Map Access</option>
										<option value="objection is not justified">objection is not justified</option>
										<option value="withdrawn by taxpayer">withdrawn by taxpayer</option>
										<option value="unilateral relief granted">unilateral relief granted</option>
										<option value="resolved via domestic remedy">resolved via domestic remedy</option>
										<option value="agreement fully eliminating double taxation/ fully resolving taxation not in accordance with tax treaty">agreement fully eliminating double taxation/ fully resolving taxation not in accordance with tax treaty</option>
										<option value="agreement that there is no taxation not in accordance with tax treaty">agreement that there is no taxation not in accordance with tax treaty</option>
										<option value="no agreement including agreement to disagree">no agreement including agreement to disagree</option>
										<option value="any other outcome">any other outcome</option>
									</select>      
								</div>
								<div class="col-md">
									<label for="case_status" class="col-form-label">Case Status</label>
									<select class="form-control" id="case_status" name="case_status">
										<option selected value="{{$data->case_status}}">{{$data->case_status}}</option>
										<option value="open">Open</option>
										<option value="close">Close</option>
									</select> 
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer text-right bg-transparent">
					{{-- <button type="button" class="btn btn-danger">Close</button> --}}
					<button type="submit" class="btn btn-primary btn-lg mt-3">Simpan</button>
				</div>
			</div>
		</form>
	</div>
	{{-- End Content Form Section --}}
@endsection

@section('script')
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<script>
		var koreksi = 0;
		$(document).ready(function() {
			$('.select2').select2({
				tags: true
			});
			$("#negara_id").change(function () {
        		var val = $(this).val();
				$('#form_negara').val(val);
			});
			$("#currency").change(function () {
        		var val = $(this).val();
				$('#form_currency').val(val);
			});
			$('#nilai_cfm_wp').change(function(){
				$('#form_nilai_cfm_wp').val($('#nilai_cfm_wp').val());
			});
			$('#nilai_cfm_pemeriksa').change(function(){
				$('#form_nilai_cfm_pemeriksa').val($('#nilai_cfm_pemeriksa').val());
				// koreksi =$('#nilai_cfm_pemeriksa').val() - $('#nilai_cfm_wp').val();
				// $('#koreksi_primer').val(koreksi);
				// $('#form_koreksi_primer').val(koreksi);
			});
			$('#koreksi_primer').change(function(){
				$('#form_koreksi_primer').val($('#koreksi_primer').val());
			});
			$('#nilai_cfm_ma').change(function(){
				$('#form_nilai_cfm_ma').val($('#nilai_cfm_ma').val());
			});
			$('#downward_adjusment').change(function(){
				$('#form_downward_adjusment').val($('#downward_adjusment').val());
			});
			$('#refund').change(function(){
				$('#form_refund').val($('#refund').val());
			});

		});

	</script>
@endsection
