@extends('layouts.form')

@section('style')

@endsection

@section('content')
<div class="row">
	@foreach($data as $value)
	<div class="col-lg-3 col-md-4 col-sm-12">
	  <div class="card card-statistic-2">
		<div class="card-icon shadow-primary bg-primary">
		  <i class="fas fa-archive"></i>
		</div>
		<div class="card-wrap">
		  <div class="card-header">
			<h4>{{$value[0]}}</h4>
		  </div>
		  <div class="card-body">
			{{$value[1]}}
		  </div>
		</div>
	  </div>
	</div>
  @endforeach
</div>
<div class="section-body">
	    <div class="row">
			<div class="col-lg-6">
				<div class="card">
					<div class="card-header">
						<h4>Overall Performance</h4>
					</div>
					<div class="card-body">
						<div class="chartjs-size-monitor" style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
							<div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
								<div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">
								</div>
							</div>
							<div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
								<div style="position:absolute;width:200%;height:200%;left:0; top:0">
								</div>
							</div>
						</div>
					<canvas id="myChart" style="display: block; width: 210px; height: 105px;" width="420" height="210" class="chartjs-render-monitor"></canvas>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
			  <div class="card gradient-bottom">
				<div class="card-header">
				  <h4>Section Performance</h4>
				  <div class="card-header-action dropdown"></div>
				</div>
				<div class="card-body" id="top-1-scroll" style="height: 315px; overflow: hidden; outline: none;" tabindex="2">
				  <ul class="list-unstyled list-unstyled-border"> 
					<li class="media">
					  <img class="mr-3 rounded" width="55" src="https://getstisla.com/dist/img/avatar/avatar-{{mt_rand(1, 5)}}.png" alt="product">
					  <div class="media-body">
						<div class="float-right">
						  <div class="font-weight-600 text-muted text-small">Section Performance</div>
						</div>
						<div class="media-title"></div>
						<div class="mt-1">
						  <div class="budget-price">
							<div class="budget-price-square bg-success" data-width="{{$seksi1 }}%" style="width: 43%;">
							</div>
							<div class="budget-price-label">{{$seksi1}}</div>
						  </div>
						  <div class="budget-price">
							<div class="budget-price-square bg-danger" data-width="{{$seksi2 }}%" style="width: 43%;">
							</div>
							<div class="budget-price-label">{{$seksi2}}</div>
						  </div>
						  <div class="budget-price">
							<div class="budget-price-square bg-info" data-width="{{$seksi3 }}%" style="width: 43%;">
							</div>
							<div class="budget-price-label">{{$seksi3}}</div>
						  </div>
						  <div class="budget-price">
							<div class="budget-price-square bg-dark" data-width="{{$seksi4 }}%" style="width: 43%;">
							</div>
							<div class="budget-price-label">{{$seksi4}}</div>
						  </div>
						</div>
					  </div>
					</li> 
				</ul>
				</div>
				<div class="card-footer pt-3 d-flex justify-content-center">
				  <div class="budget-price justify-content-center">
					<div class="budget-price-square bg-success" data-width="20" style="width: 20px;"></div>
					<div class="budget-price-label">PPSPI 1</div>
				  </div>
				  <div class="budget-price justify-content-center">
					<div class="budget-price-square bg-danger" data-width="20" style="width: 20px;"></div>
					<div class="budget-price-label">PPSPI 2</div>
				  </div>
				  <div class="budget-price justify-content-center">
					<div class="budget-price-square bg-info" data-width="20" style="width: 20px;"></div>
					<div class="budget-price-label">PPSPI 3</div>
				  </div>
				  <div class="budget-price justify-content-center">
					<div class="budget-price-square bg-dark" data-width="20" style="width: 20px;"></div>
					<div class="budget-price-label">PPSPI 4</div>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
const labels = ['MAP', 'UAPA', 'BAPA'];
const data = {
  labels: labels,
  datasets: [
    {
	  label: 'Closed Cases',
      data: [{{$close_map}}, {{$close_uapa}}, {{$close_bapa}}],
      backgroundColor: '#2596be',
    },
    {
	  label: 'Open Cases',
      data: [{{$open_map}}, {{$open_uapa}}, {{$open_bapa}}],
      backgroundColor: '#f8a42c',
    },

  ]
};

const myChart = new Chart(document.getElementById('myChart'), {
  type: 'bar',
  data: data,
  options: {
    plugins: {
    //   title: {
    //     display: true,
    //     text: 'Chart.js Bar Chart - Stacked'
    //   },
    },
    responsive: true,
    scales: {
      x: {
        stacked: true,
      },
      y: {
        stacked: true
      }
    }
  }
});
</script>	
@endsection
