<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
Use Auth;

class UserController extends Controller
{
    public function __construct()
    {
       
    }

    public function index()
    {
        if(Auth::user()->status != 2){
            return redirect()->back()->with('toast_error', 'Kamu tidak ada akses!');
        }else{
            $data = User::where('status', '!=', 2)->orderBy('created_at', 'DESC')->get();
            return view('admin.users', ['data' => $data]);
        }
    }

    public function approve($id)
    {
        if(Auth::user()->status != 2){
            return redirect()->back()->with('toast_error', 'Kamu tidak ada akses!');
        }else{
            $data = User::findOrFail($id);
            $data->status = 1;
            $data->save();
            return redirect(route('users'))->with('toast_success', "User $data->name Berhasil dijadikan Admin!");
        }
        
    }
    public function desmin($id)
    {
        if(Auth::user()->status != 2){
            return redirect()->back()->with('toast_error', 'Kamu tidak ada akses!');
        }else{
            $data = User::findOrFail($id);
            $data->status = 0;
            $data->save();
            return redirect(route('users'))->with('toast_success', "User $data->name Berhasil dihapus dari Admin!");
        }
        
    }
}
